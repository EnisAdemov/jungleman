/// <reference path = "../lib/phaser.d.ts"/>

module JungleMan {
    export class Platform {
        game: Phaser.Game;
        platformLevel: Phaser.Tilemap;
        platformLayerGround: Phaser.TilemapLayer;
        platformLayerBackground: Phaser.TilemapLayer;

        constructor(game: Phaser.Game){
            this.game = game;

            //platforms layers
            this.platformLevel = this.game.add.tilemap('level1', 32, 32);
            this.platformLevel.addTilesetImage('jungle-tileset', 'jungleTileset');
            this.platformLayerBackground = this.platformLevel.createLayer('backgroundLayer');
            this.platformLayerGround = this.platformLevel.createLayer('groundLayer');

            //platform physics
            this.game.physics.enable(this.platformLevel, Phaser.Physics.ARCADE);
            this.platformLevel.setCollisionBetween(0, 1000, true, this.platformLayerGround);
        }
    }
}