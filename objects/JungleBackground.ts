/// <reference path = "../lib/phaser.d.ts"/>

module JungleMan {
    export class JungleBackground {
        game: Phaser.Game;
        backgrounds: Array<Phaser.Sprite>;
        velocity: Array<number>;

        constructor(game: Phaser.Game, velocity: number[]) {
            this.game = game;
            this.velocity = velocity;

            var bg1 = this.game.add.sprite(0, 0, "background1");
            var bg2 = this.game.add.sprite(0, 0, "background2");
            var bg3 = this.game.add.sprite(0, 0, "background3");
            var bg4 = this.game.add.sprite(0, 0, "background4");
            var bg5 = this.game.add.sprite(0, 0, "background5");

            this.backgrounds = [];
            this.backgrounds.push(bg1);
            this.backgrounds.push(bg2);
            this.backgrounds.push(bg3);
            this.backgrounds.push(bg4);
            this.backgrounds.push(bg5);

            for(var i = 0 ; i < this.backgrounds.length ; i++) {
                this.backgrounds[i].scale.set(2.33, 3.66);
                this.backgrounds[i].anchor.set(0.3, 0);
            }
        }

        update(direction:number) {
            for (var i = 0; i < this.backgrounds.length; i++) {
                this.backgrounds[i].x -= direction*this.velocity[i];
            }
        }
    }
}