/// <reference path = "../lib/phaser.d.ts"/>

module JungleMan {
    export class Sign {
        game: Phaser.Game;
        signSprite: Phaser.Sprite;

        constructor(game: Phaser.Game, x: number, y: number){
            this.game = game;

            this.signSprite = this.game.add.sprite(x, y, 'sprSign');
            this.signSprite.anchor.set(0.5, 0.5);
            this.game.physics.enable(this.signSprite, Phaser.Physics.ARCADE);
            this.signSprite.body.setSize(32, 25);
            this.signSprite.body.velocity.x = 0;
            this.signSprite.body.velocity.y = 0;
            this.signSprite.body.immovable = true;
        }
    }
}