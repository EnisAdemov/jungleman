/// <reference path = "../lib/phaser.d.ts"/>


module JungleMan {
    export class LedgeGrab {
        game: Phaser.Game;
        grabBody: Phaser.Sprite;
        leftKey: Phaser.Key;
        rightKey: Phaser.Key;

        constructor(game: Phaser.Game){
            this.game = game;

            this.grabBody = this.game.add.sprite(150, 500, 'spr_sign');
            this.game.physics.enable(this.grabBody, Phaser.Physics.ARCADE);
            this.grabBody.scale.set(0.4, 0.4);
            this.grabBody.alpha = 0;
            this.grabBody.anchor.set(0.5, 0.5);
            this.grabBody.body.checkCollision.up = false;
            this.grabBody.body.checkCollision.right = false;
            this.grabBody.body.checkCollision.left = false;
            this.grabBody.body.checkCollision.down = true;
            this.grabBody.body.immovable = true;
            this.grabBody.body.moves = false;

            this.leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
            this.rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        }

        changeHand(direction: number) {
            if(direction === -1){
                this.grabBody.scale.setTo(-0.4, 0.4);
            }
            else {
                this.grabBody.scale.setTo(0.4, 0.4);
            }
        }
        update(playerSprite: Phaser.Sprite) {
            if (this.leftKey.isDown) {
                this.grabBody.position.x = playerSprite.position.x - 30;
                this.changeHand(-1);
            }
            else if (this.rightKey.isDown){
            this.grabBody.position.x = playerSprite.position.x + 30;
            this.changeHand(1);
            }
            this.grabBody.position.y = playerSprite.position.y - 30;
        }
    }


}