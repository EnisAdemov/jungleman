/// <reference path = "../lib/phaser.d.ts"/>

module JungleMan{
    enum PlayerState {IDLE = 1, MOVE, JUMP, GRAB};

    export class PlayerJungleMan {
        game: Phaser.Game;
        playerSprites: Phaser.Sprite;
        currectState: PlayerState;
        leftKey: Phaser.Key;
        rightKey: Phaser.Key;
        spaceKey: Phaser.Key;
        moveSpeed: number = 100;
        animationSpeed: number = 10;
        jumpHeight: number = -600;

        constructor(game: Phaser.Game, x: number, y: number) {
            this.game = game;
            this.currectState = PlayerState.IDLE;

            this.playerSprites = this.game.add.sprite(x, y, 'jungleMan');
            this.playerSprites.anchor.set(0.5, 0.5);

            //player input
            this.leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
            this.rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
            this.spaceKey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

            //adding animations idle, run, jump...json file is messed up
            this.playerSprites.animations.add('idle', [11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]);
            this.playerSprites.animations.add('run', [25, 24, 23, 22, 21, 20, 19, 18]);
            this.playerSprites.animations.add('jump', [12]);
            this.playerSprites.animations.add('ledge', [14]);

            //player's physics
            this.game.physics.enable(this.playerSprites, Phaser.Physics.ARCADE);

            //movement
            this.rightKey.onUp.add(() => {
                this.idle();
            });
            this.leftKey.onUp.add(() => {
                this.idle();
            });
            this.spaceKey.onDown.add(() => {
                this.jump();
            });

        }

         run(direction: number) {
            if(direction === 1){
                this.playerSprites.scale.setTo(1, 1);
            }
            else {
                this.playerSprites.scale.setTo(-1, 1);
            }
             this.playerSprites.body.velocity.x = direction * this.moveSpeed;

             if(this.currectState !== PlayerState.JUMP) {
                 this.currectState = PlayerState.MOVE;
                 this.playerSprites.play('run', this.animationSpeed, true);
             }

        }

        idle() {
            if (this.currectState !== PlayerState.JUMP ) {
                this.currectState = PlayerState.IDLE;
                this.playerSprites.play('idle', this.animationSpeed, true);
            }
            this.playerSprites.body.velocity.x = 0;
            this.playerSprites.body.velocity.y = 0;
            this.playerSprites.body.allowGravity = true;
        }

        ResetOnGround(){
            if(this.currectState === PlayerState.JUMP) {
                this.currectState = PlayerState.IDLE;
                this.playerSprites.play('idle', this.animationSpeed, true);
                this.playerSprites.body.velocity.x = 0;
            }
        }

        grab() {
            this.playerSprites.play('ledge', null, true);
            this.currectState = PlayerState.GRAB;
            this.playerSprites.body.velocity.y = 0;
            this.playerSprites.body.allowGravity = false;
        }

        jump(){
            if(this.currectState !== PlayerState.JUMP ) {
                this.currectState = PlayerState.JUMP;
                this.playerSprites.play('jump', null, false);
                this.playerSprites.body.velocity.y += this.jumpHeight;
                this.playerSprites.body.allowGravity = true;
            }

        }

        update() {
            if(this.currectState !== PlayerState.GRAB) {
                if (this.leftKey.isDown) {
                    this.run(-1);
                }
                else if (this.rightKey.isDown) {
                    this.run(1);
                }
            }
        }
    }
}
