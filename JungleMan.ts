/// <reference path = "lib/phaser.d.ts"/>
/// <reference path = "states/Boot.ts"/>
/// <reference path = "states/Game.ts"/>
/// <reference path = "states/Menu.ts"/>

module JungleMan {
    class JungleMan extends Phaser.Game {
        game:Phaser.Game;

        constructor(width?:number, height?:number) {
            super(width, height, Phaser.CANVAS, 'phaser-div', {create: this.create});
        }

        create() {
            this.game.state.add("Boot", Boot, false);
            this.game.state.add("Menu", Menu, false);
            this.game.state.add("Game", Game, false);

            this.game.state.start("Boot");
        }
    }

    window.onload = ()=> {
        new JungleMan(1024, 720);
    }
}