/// <reference path = "../lib/phaser.d.ts"/>
/// <reference path = "../objects/JungleBackground.ts"/>
/// <reference path = "../objects/Platform.ts"/>
/// <reference path = "../objects/PlayerJungleMan.ts"/>
/// <reference path = "../objects/Sign.ts"/>
/// <reference path = "../objects/LedgeGrab.ts"/>

module JungleMan{

    export class Game extends Phaser.State{
        background: JungleBackground;
        platform: Platform;
        jungleMan: PlayerJungleMan;
        ledgeGrab: LedgeGrab;
        sign: Sign;
        jungleManSpawnX: number = 40;
        jungleManSpawnY: number = 500;
        SignSpawnX: number = 930;
        SignSpawnY: number = 100;

        create() {
            this.camera.flash(0x000000, 1500);

            //instances
            this.background = new JungleBackground(this.game, [0.001, 0.002, 0.003, 0.004, 0.005]);
            this.platform = new Platform(this.game);
            this.jungleMan = new PlayerJungleMan(this.game, this.jungleManSpawnX, this.jungleManSpawnY);
            this.ledgeGrab = new LedgeGrab(this.game);
            this.sign = new Sign(this.game, this.SignSpawnX, this.SignSpawnY);

            //game physics
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 2000;
            this.jungleMan.playerSprites.body.collideWorldBounds = true;
        }

        update(){
            //collsions
            this.game.physics.arcade.collide(this.jungleMan.playerSprites, this.platform.platformLayerGround,()=>{
                this.jungleMan.ResetOnGround();
            });

            this.game.physics.arcade.collide(this.sign.signSprite, this.platform.platformLayerGround);
            this.game.physics.arcade.collide(this.jungleMan.playerSprites, this.sign.signSprite, ()=> {
                this.game.camera.flash(0x000000, 5000);
                this.game.add.text(this.game.world.centerX, this.game.world.centerY, "I'm going to write lorem ipsum here...");
            });
            this.game.physics.arcade.collide(this.ledgeGrab.grabBody, this.platform.platformLayerGround,()=>{
                this.jungleMan.grab();
            });

            //if player falls down in the pit, reset his possition
            if(this.jungleMan.playerSprites.y + (this.jungleMan.playerSprites.height/2) + 5 >= this.world.height){
                this.jungleMan.playerSprites.x = this.jungleManSpawnX;
                this.jungleMan.playerSprites.y = this.jungleManSpawnY;
            }

            //movement
            this.jungleMan.update();
            this.background.update(this.jungleMan.playerSprites.body.velocity.x);
            this.ledgeGrab.update(this.jungleMan.playerSprites);
        }
    }
}