/// <reference path = "../lib/phaser.d.ts"/>
/// <reference path = "../objects/PlayerJungleMan.ts"/>

module JungleMan {
    export class Menu extends Phaser.State {
        logo: Phaser.Sprite;
        platformLevel: Phaser.Tilemap;
        platformLayerGround: Phaser.TilemapLayer;
        platformLayerArrow: Phaser.TilemapLayer;
        platformLayerDoor: Phaser.TilemapLayer;
        jungleMan: PlayerJungleMan;

        create() {
            this.jungleMan = new PlayerJungleMan(this.game, 50, 50);

            this.logo = this.game.add.sprite(220,90 , "sprLogo");
            this.platformLevel = this.game.add.tilemap('menuLevel', 32, 32);
            this.platformLevel.addTilesetImage('jungle-tileset', 'jungleTileset');
            this.platformLayerArrow= this.platformLevel.createLayer('arrow');
            this.platformLayerDoor = this.platformLevel.createLayer('doorLayer');
            this.platformLayerGround = this.platformLevel.createLayer('groundLayer');

            //physics
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 2000;
            this.game.physics.enable(this.platformLevel, Phaser.Physics.ARCADE);
            this.platformLevel.setCollisionBetween(0, 1000, true, this.platformLayerGround);
            this.platformLevel.setCollisionBetween(0, 1000, true, this.platformLayerDoor);

            this.game.stage.backgroundColor = "#182c3b";
        }

        update() {
            this.game.physics.arcade.collide(this.jungleMan.playerSprites, this.platformLayerGround,()=>{
                this.jungleMan.ResetOnGround();
            });

            this.game.physics.arcade.collide(this.jungleMan.playerSprites, this.platformLayerDoor,()=>{
                this.game.state.start("Game");
            });

            //collision between player and groundLayer
            this.jungleMan.update();
        }
    }
}