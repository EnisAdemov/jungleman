/// <reference path = "../lib/phaser.d.ts"/>

module JungleMan {
    export class Boot extends Phaser.State {

        preload() {
            //Parallax backgrounds
            this.game.load.image("background1","assets/Graphics/Stage/plx-1.png");
            this.game.load.image("background2","assets/Graphics/Stage/plx-2.png");
            this.game.load.image("background3","assets/Graphics/Stage/plx-3.png");
            this.game.load.image("background4","assets/Graphics/Stage/plx-4.png");
            this.game.load.image("background5","assets/Graphics/Stage/plx-5.png");

            //Jungle Tileset
            this.game.load.image('jungleTileset', "assets/Graphics/Stage/jungle-tileset.png");
            this.game.load.tilemap('level1', 'assets/Graphics/Stage/level1.json', null, Phaser.Tilemap.TILED_JSON);
            this.game.load.tilemap('menuLevel', 'assets/Graphics/Stage/menuLevel.json', null, Phaser.Tilemap.TILED_JSON);

            //Character Spritesheet
            this.game.load.atlas('jungleMan', "assets/Graphics/JungleMan/jungleman.png", "assets/Graphics/JungleMan/jungleman.json");

            //Character Grab
            this.game.load.image("sprLedge", "assets/Graphics/JungleMan/ledge grab outline.png");

            //Sign Sprite
            this.game.load.image("sprSign","assets/Graphics/Sign/spr_sign.png");

            //Logo Sprite
            this.game.load.image("sprLogo", "assets/Graphics/Menu/spr_Menu.png");
        }

        create() {
            this.game.state.start("Menu");
        }
    }
}